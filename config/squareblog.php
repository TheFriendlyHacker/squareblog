<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Blog File Locations
	|--------------------------------------------------------------------------
	|
	| Specify the locations/folders in which various types of images for blog
	| should be stored.
	|
	| Will be accessible via <base url>/storage/<specified location/<file name>
	|
	*/
	'blog_file_location' => 'public/posts',
	'settings_file_location' => 'public/settings',
];