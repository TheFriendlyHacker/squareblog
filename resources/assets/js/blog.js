$(document).ready(function() {

	/**
	 * Initalize Bootstrap tooltips and popovers
	 */
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();

	/**
	 * Listen for form elements with the "swal" class to be clicked.
	 */
	$('form.swal').on('click submit', function(event, b, c) {
		event.preventDefault();
		var form = $(this);
		var title = form.data('swal-title');
		var text = form.data('swal-text');

		swal({
		  title: title,
		  text: text,
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, I am sure",
		  closeOnConfirm: false,
	    showLoaderOnConfirm: true
		},
		function(){
			form.off('submit');
			form.submit();
		});
	});


	/**
	 * Initialize any markdown editors on the page.
	 * @link https://github.com/sparksuite/simplemde-markdown-editor
	 */
	if($('textarea.mde')[0])
	{
		$('textarea.mde').each(function(index, element) {
			var simplemde = new SimpleMDE({ element: element });
		});
	}


	/**
	 * Initialize any fancy file inputs on the page
	 * @link https://github.com/kartik-v/bootstrap-fileinput
	 */
	if($('input.bs-file-input')[0]) {
		$('input.bs-file-input').each(function(index, element) {
			var target = $(element);
			$(element).fileinput();
		});
	}

	/**
	 * Listen for the user clicking on an element with the "logout" class.
	 * This will automatically create a form and submit it with a "POST" request,
	 * allowing them to logout.
	 */
	if($('.logout')[0]) {
		$('.logout').each(function(index, element) {
			element = $(element);
			element.on('click submit', function(event) {
				event.preventDefault();
				var action = $(this).attr('href');

				var form = new InvisibleForm();
				form.method('POST').action(action).append('body').send();
			});
		});
	}

});




/**
 * Create a new "invisible" form. This is useful for executing actions which require POST/PUT/DELETE
 * requests, without having to include a form in the view. For example, you could use this to post a DELETE
 * request when a button is clicked. Normally, a delete request must be submitted with a form.
 */
var InvisibleForm = function()
{
	// The form element
	this._form = null;
	// The form's action
	this._action = null;
	// The form's method
	this._method = 'GET';
	/**
	 * Add a hidden input field to the form.
	 * @param {string} name
	 * @param {string} value
	 * @return {this}
	 */
	this.addField = function(name, value)
	{
		var field;
		if(this._form.find("input[name='"+name+"']").length > 0) {
			field = $(this._form.find("input[name='"+name+"']")[0]);
		} else {
			field = $('<input />');
		}

		field.attr('type', 'hidden')
					.attr('name', name)
					.attr('value', value);

		this._form.append(field);

		return this;
	};
	/**
	 * Remove the given field from the form.
	 * @param  {string} name
	 * @return {this}
	 */
	this.removeField = function(name)
	{
		var field = $(this._form.find("input[name='"+name+"']"));
		if(field.length > 0) {
			field.remove();
		}
	};
	/**
	 * Specify the method to use (GET/POST/PUT/DELETE).
	 * @param  {string} method
	 * @return {this}
	 */
	this.method = function(method)
	{
		var postMethods = ['post', 'put', 'delete'];
		this._method = method;
		if(postMethods.indexOf(method.toLowerCase()) !== -1) {
			this._form.attr('method', 'POST');
			this.addField('_method', method);
			this.addField('_token', SquareBlog.csrfToken);
		} else {
			this._form.attr('method', 'GET');
			this.removeField('_method');
			this.removeField('_token');
		}
		return this;
	};
	/**
	 * Specify the action/url for the form.
	 * @param  {string} action
	 * @return {this}
	 */
	this.action = function(action)
	{
		this._action = action;
		this._form.attr('action', action);

		return this;
	};
	/**
	 * Append the form to the given element. Usually the body.
	 * @param  {element} target
	 * @return {this}
	 */
	this.append = function(target)
	{
		$(target).append(this._form);
		return this;
	};
	/**
	 * Submit the form.
	 * @return {void}
	 */
	this.send = function()
	{
		this._form.submit();
	};
	/**
	 * Class constructor.
	 * @return {this}
	 */
	this._construct = function()
	{
		var form = $('<form />');
		form.attr('id', 'invisible-form');
		this._form = form;
		this.send();
		this.addField('_method', this._method);
		return this;
	};
	// Construct the class
	this._construct();
};