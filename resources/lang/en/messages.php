<?php
return [
	'user' => [
		'profile-successfully-updated' => 'Your profile has been updated!',
		'current-password-invalid' => 'The "current password" you entered is invalid.',
		'password-successfully-changed' => 'Your password has been changed!',
	],

	'posts' => [
		'successfully-created' => 'Your post was created successfully!',
		'successfully-updated' => 'Your post was successfully updated!',
		'successfully-deleted' => 'Your post has been deleted!',
	],

	'comments' => [
		'successfully-created' => 'Your comment has been posted!',
		'successfully-deleted' => 'Your comment has been deleted!',
	],

	'contact' => [
		'request_sent' => 'Thank you, your message has been received!'
	],

	'admin' => [
		'settings-updated' => 'Website settings updated',
		'contact-updated' => 'Contact page settings updated',
		'about-updated' => 'About page settings updated',
		'errors' => [
			'contact_email_required_with_page' => 'You must specify a contact email address in order to display the contact page.',
			'about_title_required_with_page' => 'You must specify a page title in order to display the about page.',
			'about_body_required_with_page' => 'You must include a description of yourself/your webiste in order to display the about page.'
		]
	],
];