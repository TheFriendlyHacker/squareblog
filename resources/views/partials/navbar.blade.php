<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('home') }}">{{ settings('app_name') }}</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-left">
        <li><a href="{{ route('home') }}">Home</a></li>

        @if(settings('show_about_page'))
        <li><a href="{{ route('about') }}">About</a></li>
        @endif
        @if(settings('show_contact_page'))
        <li><a href="{{ route('contact') }}">Contact</a></li>
        @endif
      </ul>

      <ul class="nav navbar-nav navbar-right">

        @if(auth()->guest())
        <li><a href="{{ route('login') }}">Sign In</a></li>
        <li><a href="{{ route('register') }}">Sign Up</a></li>
        @else
        <li class="dropdown">
          <a href="{{ route('me.profile') }}" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-user"></i> {{ auth()->user()->name }} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('me.profile') }}"><i class="fa fa-user"></i> My Account</a></li>
            <li><a href="{{ route('logout') }}" class="logout"><i class="fa fa-sign-out"></i> Logout</a></li>
          </ul>
        </li>
        @endif
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>