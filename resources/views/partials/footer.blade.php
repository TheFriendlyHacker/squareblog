<!-- Footer -->
<footer>
  <div class="row">
    <div class="col-lg-12">
      <p>Copyright &copy; {{ settings('app_name') }} {{ date('Y') }}</p>
    </div>
  </div>
</footer>