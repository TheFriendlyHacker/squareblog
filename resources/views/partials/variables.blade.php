<script type="text/javascript">
window.SquareBlog = {
	'base_url': "{{ env('APP_URL') }}",
	'app_name': "{{ Settings::get('app_name', 'SquareBlog') }}",
	'csrfToken': "{{ csrf_token() }}",
};
</script>