@extends('layouts.app')
@section('page-title', ' :: Contact')

@section('content')
<div class="container">
  <!-- Page Heading/Breadcrumbs -->
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Contact
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a>
        </li>
        <li class="active">Contact</li>
      </ol>
    </div>
  </div>
  <!-- /.row -->


  <!-- Contact Form -->
  <div class="row">
    <div class="col-md-8 col-md-offset-2">

			@include('components.success-alert')
			@include('components.error-alert')

      <h3>Send us a Message</h3>
      <form action="{{ route('contact') }}" method="POST" role="form">
      	{!! csrf_field() !!}

      	<!-- Full Name -->
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label">Full Name:</label>
          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
          @if ($errors->has('name'))
          <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
          @endif
        </div>

      	<!-- Email Address -->
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <label for="email" class="control-label">Email Address:</label>
          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
          @if ($errors->has('email'))
          <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
          @endif
        </div>

				<!-- Phone Number -->
      	<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
      	  <label for="phone" class="control-label">Phone Number <small>(optional)</small>:</label>
      	  <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
      	  @if ($errors->has('phone'))
      	  <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
      	  @endif
      	</div>

        <!-- Message -->
        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
          <label for="phone" class="control-label">Message:</label>
          <textarea name="message" id="message" rows="10" class="form-control">{{ old('message') }}</textarea>
          @if ($errors->has('message'))
          <span class="help-block"><strong>{{ $errors->first('message') }}</strong></span>
          @endif
        </div>

        <button type="submit" class="btn btn-primary">Send Message</button>
      </form>
    </div>
  </div>
</div>
@endsection