@extends('layouts.app')
@section('page-title', ' :: About')

@section('content')
<div class="container">
  <!-- Page Heading/Breadcrumbs -->
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">About
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a>
        </li>
        <li class="active">About</li>
      </ol>
    </div>
  </div>
  <!-- /.row -->
  <!-- Intro Content -->
  <div class="row">
  	@if(isset($about->about_image))
    <div class="col-md-6">
      <img class="img-responsive" src="{{ $about->about_image }}" alt="About {{ settings('app_name') }}">
    </div>
    @endif
    <div class="col-md-{{ isset($about->about_image) ? '6' : '12' }}">
      <h2>{{ $about->about_title }}</h2>
      @markdown($about->about_body)
    </div>
  </div>
  <!-- /.row -->
  <hr>
</div>
@endsection