@component('mail::message')
# {{ $data['name'] }} sent you a contact request

@component('mail::table')
|        |          |   |
| -------------- |-------------:|
| Full Name      | {{ $data['name'] }}      |
| Email Address  | {{ $data['email'] }}    |
| Phone Number   | {{ $data['phone'] or 'Not Given' }} |
@endcomponent

@component('mail::panel')
{{ $data['message'] }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent