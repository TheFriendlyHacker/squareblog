<div class="row blog-post-home-preview">

  <!-- Timestamp -->
  <div class="col-md-1 text-center">
    <p><i class="fa fa-calendar fa-4x"></i>
    </p>
    <p><strong>{{ $post->created_at->format('M d, Y') }}</strong></p>
  </div>

  <!-- Blog Thumbnail Image -->
  @if(isset($post->thumbnail))
  <div class="col-md-5 text-center">
    <a href="{{ route('post.show', $post->id) }}">
      <img src="{{ asset($post->thumbnail) }}" alt="{{ $post->title }}" class="img-responsive img-hover blog-thumbnail" align="right">
    </a>
  </div>
  @endif

  <!-- Blog Preview -->
  <div class="col-md-{{ isset($post->thumbnail) ? '6' : '11' }}">
    <h3>
      <a href="{{ route('post.show', $post->id) }}">{{ $post->title }}</a>
    </h3>
    <p>by <a href="{{ route('user.profile', $post->user->id) }}">{{ $post->user->name }}</a>
    </p>
    <p>{{ str_limit(strip_tags(Markdown::parse($post->body)), isset($post->thumbnail) ? '636' : '318') }}</p>
    <a class="btn btn-primary" href="{{ route('post.show', $post->id) }}">Read More <i class="fa fa-angle-right"></i></a>
  </div>
</div>
<hr>