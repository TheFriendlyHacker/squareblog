@if(count($errors) > 0)
<div class="alert alert-sm alert-border-left alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <i class="fa fa-times-circle pr10"></i>
  <strong>Please correct the following errors: </strong>
  <br><br>
  <ul class="list-unstyled">
    @foreach ($errors->all() as $error)
      <li><i class="fa fa-times"></i>&nbsp;&nbsp;{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif