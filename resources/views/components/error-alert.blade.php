@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<strong><i class="fa fa-times-circle"></i> {{ Session::get('error') }}</strong>
</div>
@endif