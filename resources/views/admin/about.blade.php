@extends('admin.layout')
@section('page-title', ' :: Admin - About Page')
@section('breadcrumb', 'About Settings')

@push('stylesheets')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
@endpush
@push('scripts')
<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
@endpush

@section('admin-content')
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> About Page</h3>
	</div>
	<div class="panel-body">
		<form action="{{ route('admin.about') }}" method="POST" role="form" class="" enctype="multipart/form-data">
			{!! method_field('PUT') !!}
			{!! csrf_field() !!}

			<!-- Show About Page -->
			<div class="form-group{{ $errors->has('show_about_page') ? ' has-error' : '' }}">
				<input type="hidden" name="show_about_page" value="0">
		    <div class="checkbox">
		      <label>
		      <input type="checkbox" name="show_about_page" value="1" {{ old('show_about_page', $settings->show_about_page) ? 'checked' : '' }}>
			      Display About Page
		      </label>
		    </div>
		    @if ($errors->has('show_about_page'))
		    <span class="help-block"><strong>{{ $errors->first('show_about_page') }}</strong></span>
		    @endif
			</div>

			<!-- About Page Title -->
			<div class="form-group{{ $errors->has('about_title') ? ' has-error' : '' }}">
			  <label for="about_title" class="control-label">Page Title</label>
		    <input id="about_title" type="text" class="form-control" name="about_title" value="{{ old('about_title', $settings->about_title) }}">
		    @if ($errors->has('about_title'))
		    <span class="help-block"><strong>{{ $errors->first('about_title') }}</strong></span>
		    @endif
			</div>

			<!-- About Paragraph -->
			<div class="form-group{{ $errors->has('about_title') ? ' has-error' : '' }}">
				<label for="about_body" class="control-label">Describe Yourself or Your Website</label>
				<textarea name="about_body" id="about_body" rows="10" class="mde">{{ old('about_body', $settings->about_body) }}</textarea>
		    @if ($errors->has('about_body'))
		    <span class="help-block"><strong>{{ $errors->first('about_body') }}</strong></span>
		    @endif
			</div>

			<!-- About Page Image -->
			<div class="form-group{{ $errors->has('about_image') ? ' has-error' : '' }}">
			  <label for="about_image" class="control-label">Picture (optional)</label>
		    <input id="about_image" type="file" class="form-control" name="about_image">
		    <span class="help-block"><strong><i class="fa fa-exclamation-triangle"></i> Min. size 750px x 450px</strong></span>

				@if(isset($settings->about_image))
				<div class="row">
					<div class="col-md-6">
						<img src="{{ asset($settings->about_image) }}" alt="About Image" class="img-responsive">
					</div>
				</div>
				@endif

			</div>

			<hr>

			<!-- Save/Cancel Buttons -->
			<div class="form-group">
			  <div class="col-md-8 col-md-offset-4">
			    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save Changes</button>
			    <a href="{{ route('admin.about') }}" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
			  </div>
			</div>

		</form>
	</div>
</div>
@endsection