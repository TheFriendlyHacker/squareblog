@extends('admin.layout')
@section('page-title', ' :: Admin - Settings')
@section('breadcrumb', 'Blog Settings')

@section('admin-content')
<!-- General Settings -->
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-dashboard"></i> Settings</h3>
	</div>
	<div class="panel-body">
		<form action="{{ route('admin.settings') }}" method="POST" role="form" class="form-horizontal">
			{!! method_field('PUT') !!}
			{!! csrf_field() !!}

			<!-- App Name -->
			<div class="form-group{{ $errors->has('app_name') ? ' has-error' : '' }}">
			  <label for="app_name" class="col-md-4 control-label">Website Name</label>
			  <div class="col-md-6">
			    <input id="app_name" type="text" class="form-control" name="app_name" value="{{ old('app_name', $settings->app_name) }}">
			    @if ($errors->has('app_name'))
			    <span class="help-block"><strong>{{ $errors->first('app_name') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Home Page Title -->
			<div class="form-group{{ $errors->has('home_title') ? ' has-error' : '' }}">
			  <label for="home_title" class="col-md-4 control-label">Home Title</label>
			  <div class="col-md-6">
			    <input id="home_title" type="text" class="form-control" name="home_title" value="{{ old('home_title', $settings->home_title) }}">
			    @if ($errors->has('home_title'))
			    <span class="help-block"><strong>{{ $errors->first('home_title') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Website Theme -->
			<div class="form-group{{ $errors->has('app_theme') ? ' has-error' : '' }}">
			  <label for="app_theme" class="col-md-4 control-label">Website Theme</label>
			  <div class="col-md-6">
			  	<select name="app_theme" id="app_theme" class="form-control">
			  		@foreach($themes as $theme)
						<option value="{{ $theme }}" @if($settings->app_theme === $theme) selected @endif>{{ ucfirst($theme) }}</option>
			  		@endforeach
			  	</select>
			    @if ($errors->has('app_theme'))
			    <span class="help-block"><strong>{{ $errors->first('app_theme') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Allow Comments -->
			<div class="form-group{{ $errors->has('allow_comments') ? ' has-error' : '' }}">
				<input type="hidden" name="allow_comments" value="0">
			  <div class="col-md-6 col-md-offset-4">
			    <div class="checkbox">
			      <label>
			      <input type="checkbox" name="allow_comments" value="1" {{ old('allow_comments', $settings->allow_comments) ? 'checked' : '' }}> Allow Blog Comments
			      </label>
			    </div>
			    @if ($errors->has('allow_comments'))
			    <span class="help-block"><strong>{{ $errors->first('allow_comments') }}</strong></span>
			    @endif
			  </div>
			</div>

			<hr>

			<!-- Email Settings -->
			<p class="lead">Email Settings</p>
			<!-- Email "From" Address -->
			<div class="form-group{{ $errors->has('from_email') ? ' has-error' : '' }}">
			  <label for="from_email" class="col-md-4 control-label">
			  	From Email
			  	<i class="fa fa-question-circle"
			  			data-toggle="tooltip"
			  			title="This is the email address that emails from your site will be sent from. It is best to use something like 'no-reply@yourdomain.com' for this.">
	  			</i>
		  	</label>
			  <div class="col-md-6">
			    <input id="from_email" type="text" class="form-control" name="from_email" value="{{ old('from_email', $settings->from_email) }}">
			    @if ($errors->has('from_email'))
			    <span class="help-block"><strong>{{ $errors->first('from_email') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Email "From" Name -->
			<div class="form-group{{ $errors->has('from_name') ? ' has-error' : '' }}">
			  <label for="from_name" class="col-md-4 control-label">
			  	From Name
			  	<i class="fa fa-question-circle"
			  			data-toggle="tooltip"
			  			title="This is the name that will appear on emails your users receive. It is best to use the name of your website or your 'real' name for this.">
	  			</i>
		  	</label>
			  <div class="col-md-6">
			    <input id="from_name" type="text" class="form-control" name="from_name" value="{{ old('from_name', $settings->from_name) }}">
			    @if ($errors->has('from_name'))
			    <span class="help-block"><strong>{{ $errors->first('from_name') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Save/Cancel Buttons -->
			<div class="form-group">
			  <div class="col-md-8 col-md-offset-4">
			    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save Changes</button>
			    <a href="{{ route('admin.settings') }}" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
			  </div>
			</div>

		</form>
	</div>
</div>
@endsection