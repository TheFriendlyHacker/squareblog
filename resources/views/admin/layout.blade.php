@extends('layouts.app')

@section('content')
<div class="container">
  <!-- Page Heading/Breadcrumbs -->
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">
      	Manage Your Blog
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a>
        </li>
        <li class="active">Admin</li>
        <li class="active">@yield('breadcrumb', 'Manage Blog')</li>
      </ol>
    </div>
  </div>

	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-bank"></i> Admin Stuff</h3>
				</div>
				<div class="panel-body user-profile-sidebar">
					<div class="list-group">
					  <a href="{{ route('admin.settings') }}" class="list-group-item {{ active('admin', true) }}">
					  	<i class="fa fa-cogs"></i> Settings
				  	</a>
		  		  <a href="{{ route('admin.users') }}" class="list-group-item {{ active('admin/users') }}">
		  		  	<i class="fa fa-users"></i> Users
		  	  	</a>
		  		  <a href="{{ route('admin.about') }}" class="list-group-item {{ active('admin/about') }}">
		  		  	<i class="fa fa-pencil"></i> About Page
		  	  	</a>
  	  		  <a href="{{ route('admin.contact') }}" class="list-group-item {{ active('admin/contact') }}">
  	  		  	<i class="fa fa-envelope"></i> Contact Page
  	  	  	</a>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			@include('components.success-alert')
			@include('components.error-alert')
			@include('components.form-errors-alert')

			@yield('admin-content')
		</div>
	</div>

</div>

@endsection