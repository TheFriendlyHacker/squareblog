@extends('admin.layout')
@section('page-title', ' :: Admin - Users')
@section('breadcrumb', 'Manage Users')

@section('admin-content')

<!-- User Search -->
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-search"></i> Search Users</h3>
	</div>
	<div class="panel-body">
		<form action="{{ route('admin.users') }}" method="GET">
		  <div class="input-group">
		    <input type="text" name="q" class="form-control" placeholder="Enter a name or email address" value="{{ request('q') }}" autofocus>
		    <span class="input-group-btn">
		      <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
		    </span>
		  </div>
		</form>
		@if(request()->has('q'))
		<a href="{{ route('admin.users') }}" class="btn btn-xs btn-default"><i class="fa fa-times"></i> Clear Search</a>
		@endif
	</div>
</div>

<!-- User Listing -->
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-users"></i> Users <strong>({{ $user_count }})</strong></h3>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><strong>Name</strong></th>
						<th><strong>Email</strong></th>
						<th><strong>Sign Up Date</strong></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td class="pd-t-14 pd-b-14">{{ $user->name }}</td>
						<td class="pd-t-14 pd-b-14">{{ $user->email }}</td>
						<td class="pd-t-14 pd-b-14">{{ $user->created_at->format('M d, Y') }}</td>
						<td class="pd-t-10 text-right">
							<a href="{{ route('user.profile', $user->id) }}" class="btn btn-primary btn-sm" target="_blank">
								<i class="fa fa-eye"></i> View Profile
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<!-- Pagination Links -->
		<div class="text-center">
			{{ $users->links() }}
		</div>

	</div>
</div>
@endsection