@extends('admin.layout')
@section('page-title', ' :: Admin - Contact Page')
@section('breadcrumb', 'Contact Settings')

@section('admin-content')
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-envelope"></i> Contact Page</h3>
	</div>
	<div class="panel-body">
		<form action="{{ route('admin.contact') }}" method="POST" role="form" class="form-horizontal">
			{!! method_field('PUT') !!}
			{!! csrf_field() !!}

			<!-- Show Contact Page -->
			<div class="form-group{{ $errors->has('show_contact_page') ? ' has-error' : '' }}">
				<input type="hidden" name="show_contact_page" value="0">
			  <div class="col-md-6 col-md-offset-4">
			    <div class="checkbox">
			      <label>
			      <input type="checkbox" name="show_contact_page" value="1" {{ old('show_contact_page', $settings->show_contact_page) ? 'checked' : '' }}>
				      Display Contact Page
			      </label>
			    </div>
			    @if ($errors->has('show_contact_page'))
			    <span class="help-block"><strong>{{ $errors->first('show_contact_page') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Contact Email -->
			<div class="form-group{{ $errors->has('contact_email') ? ' has-error' : '' }}">
			  <label for="contact_email" class="col-md-4 control-label">Contact Email</label>
			  <div class="col-md-6">
			    <input id="contact_email" type="email" class="form-control" name="contact_email" value="{{ old('contact_email', $settings->contact_email) }}">
			    @if ($errors->has('contact_email'))
			    <span class="help-block"><strong>{{ $errors->first('contact_email') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Save/Cancel Buttons -->
			<div class="form-group">
			  <div class="col-md-8 col-md-offset-4">
			    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save Changes</button>
			    <a href="{{ route('admin.contact') }}" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
			  </div>
			</div>

		</form>
	</div>
</div>
@endsection