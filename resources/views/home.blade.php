@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">
        {{ Settings::get('home_title', '') }}
      </h1>
      @if(auth()->check() && auth()->user()->is_admin)
      <a href="{{ route('post.create') }}" class="btn btn-primary mr-b-20"><i class="fa fa-plus"></i> New Blog Post</a>
      @endif
      <ol class="breadcrumb">
        <li class="active">Home</li>
      </ol>
    </div>
  </div>
  <!-- /.row -->

  <!-- Session Messages (if applicable) -->
  <div class="row">
    @include('components.success-alert')
    @include('components.error-alert')
  </div>

  <!-- Blog Search -->
  <div class="panel panel-default">
    <div class="panel-body">
      <form action="{{ route('post.search') }}" method="GET">
        <div class="input-group">
          <input type="text" name="q" class="form-control"  placeholder="Blog search...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </form>
    </div>
  </div>

  <hr>

  <!-- Blog Posts -->
  @foreach($posts as $post)
    @component('components.posts.blog-post-preview', ['post' => $post]) @endcomponent
  @endforeach

  <hr>

  <!-- Pagination Links -->
  <div class="row text-center">
    {{ $posts->links() }}
  </div>

  <hr>
</div>
@endsection
