@extends('me.user')
@section('page-title', ' :: Your Profile')

@section('user-content')

	@include('components.success-alert')
	@include('components.error-alert')

	<!-- User's Contact Information -->
	@include('me.profile.contact')

	<!-- User's "About Me" -->
	@include('me.profile.about')

@endsection