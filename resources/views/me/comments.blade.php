@extends('me.user')
@section('page-title', ' :: Your Comments')

@section('user-content')

	@include('components.success-alert')
	@include('components.error-alert')

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-comments"></i> Your Comments <strong>({{ $user->comments->count() }})</strong></h3>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th><strong>Post</strong></th>
							<th><strong>Comment</strong></th>
							<th><strong>Date</strong></th>
						</tr>
					</thead>
					<tbody>
						@forelse($comments as $comment)
						<tr>
							<td class="pd-t-14 pd-b-14">
								<a href="{{ route('post.show', $comment->post->id) }}">{{ str_limit($comment->post->title, 64) }}</a>
							</td>
							<td class="pd-t-14 pd-b-14">
								{{ str_limit($comment->body, 100) }}
							</td>
							<td class="pd-t-14 pd-b-14">
								{{ $comment->created_at->format('M d, Y') }}
							</td>
						</tr>
						@empty
						<tr>
							<td class="pd-t-30 pd-b-30 text-center">
								<strong><em>You have not posted any comments</em></strong>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
			<div class="text-center">
				{{ $comments->links() }}
			</div>
		</div>
	</div>

@endsection