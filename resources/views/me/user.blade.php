@extends('layouts.app')

@section('content')
<div class="container">
  <!-- Page Heading/Breadcrumbs -->
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">
      	{{ auth()->user()->name }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a>
        </li>
        <li class="active">Your Information</li>
      </ol>
    </div>
  </div>

	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-id-card"></i> Your Stuff</h3>
				</div>
				<div class="panel-body user-profile-sidebar">
					<div class="list-group">
					  <a href="{{ route('me.profile') }}" class="list-group-item {{ active('me', true) }}">
					  	<i class="fa fa-user"></i> Profile
				  	</a>
					  <a href="{{ route('me.security') }}" class="list-group-item {{ active('me/security') }}">
					  	<i class="fa fa-lock"></i> Security
				  	</a>
		  	  	@if($user->is_admin)
		  		  <a href="{{ route('me.posts') }}" class="list-group-item {{ active('me/posts') }}">
		  		  	<i class="fa fa-edit"></i> My Posts
		  	  	</a>
		  	  	@endif
					  <a href="{{ route('me.comments') }}" class="list-group-item {{ active('me/comments') }}">
					  	<i class="fa fa-comments"></i> My Comments
				  	</a>
		  	  	@if($user->is_admin)
		  		  <a href="{{ route('admin.settings') }}" class="list-group-item {{ active('admin', true) }}">
		  		  	<i class="fa fa-edit"></i> Manage Blog
		  	  	</a>
		  	  	@endif
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			@yield('user-content')
		</div>
	</div>

</div>

@endsection