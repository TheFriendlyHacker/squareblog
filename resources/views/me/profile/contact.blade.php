<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-user"></i> Contact Information</h3>
	</div>
	<div class="panel-body">

		<form action="{{ route('me.profile.update') }}" method="POST" role="form" class="form-horizontal">
			{!! method_field('PUT') !!}
			{!! csrf_field() !!}

			<!-- First Name -->
			<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
			  <label for="first_name" class="col-md-4 control-label">First Name</label>
			  <div class="col-md-6">
			    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
			    @if ($errors->has('first_name'))
			    <span class="help-block"><strong>{{ $errors->first('first_name') }}</strong></span>
			    @endif
			  </div>
			</div>
			<!-- Last Name -->
			<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
			  <label for="last_name" class="col-md-4 control-label">Last Name</label>
			  <div class="col-md-6">
			    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
			    @if ($errors->has('last_name'))
			    <span class="help-block"><strong>{{ $errors->first('last_name') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Email Address -->
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			  <label for="email" class="col-md-4 control-label">E-Mail Address</label>
			  <div class="col-md-6">
			    <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">
			    @if ($errors->has('email'))
			    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Save Button -->
			<div class="form-group">
			  <div class="col-md-8 col-md-offset-4">
			    <button type="submit" class="btn btn-primary">
				    <i class="fa fa-check-circle"></i> Save Changes
			    </button>
			  </div>
			</div>

		</form>

	</div>
</div>