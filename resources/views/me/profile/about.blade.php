<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> About You</h3>
	</div>
	<div class="panel-body">

		<form action="{{ route('me.profile.about.update') }}" method="POST" role="form" class="">
			{!! method_field('PUT') !!}
			{!! csrf_field() !!}

			<div class="form-group">
				<textarea name="about" id="about" rows="10" class="form-control">{{ $user->about }}</textarea>
			</div>

			<!-- Save Button -->
			<div class="form-group">
			  <div class="col-md-8 col-md-offset-4">
			    <button type="submit" class="btn btn-primary">
				    <i class="fa fa-check-circle"></i> Save Changes
			    </button>
			  </div>
			</div>

		</form>

	</div>
</div>