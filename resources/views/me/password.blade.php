@extends('me.user')
@section('page-title', ' :: Change Your Password')

@section('user-content')

@include('components.success-alert')
@include('components.error-alert')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-lock"></i> Change Password</h3>
	</div>
	<div class="panel-body">

		<form action="{{ route('me.security') }}" method="POST" role="form" class="form-horizontal">
			{!! method_field('PUT') !!}
			{!! csrf_field() !!}

			<!-- Current Password -->
			<div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
			  <label for="current_password" class="col-md-4 control-label">Current Password</label>
			  <div class="col-md-6">
			    <input id="current_password" type="password" class="form-control" name="current_password">
			    @if ($errors->has('current_password'))
			    <span class="help-block"><strong>{{ $errors->first('current_password') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Current Password -->
			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			  <label for="password" class="col-md-4 control-label">New Password</label>
			  <div class="col-md-6">
			    <input id="password" type="password" class="form-control" name="password">
			    @if ($errors->has('password'))
			    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Current Password Confirmation -->
			<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
			  <label for="password_confirmation" class="col-md-4 control-label">Confirm Password</label>
			  <div class="col-md-6">
			    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation">
			    @if ($errors->has('password_confirmation'))
			    <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
			    @endif
			  </div>
			</div>

			<!-- Save Button -->
			<div class="form-group">
			  <div class="col-md-8 col-md-offset-4">
			    <button type="submit" class="btn btn-primary">
				    <i class="fa fa-check-circle"></i> Change Password
			    </button>
			  </div>
			</div>

		</form>

	</div>
</div>

@endsection