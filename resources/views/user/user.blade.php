@extends('layouts.app')

@section('content')
<div class="container">
  <!-- Page Heading/Breadcrumbs -->
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">
      	{{ $user->name }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a>
        </li>
        <li class="active">Users</li>
        <li class="active">{{ $user->name }}</li>
      </ol>
    </div>
  </div>

	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-id-card"></i> {{ $user->first_name }}'s Stuff</h3>
				</div>
				<div class="panel-body user-profile-sidebar">
					<div class="list-group">
					  <a href="{{ route('user.profile', $user->id) }}" class="list-group-item {{ active('user/'.$user->id, true) }}">
					  	<i class="fa fa-user"></i> Profile
				  	</a>
				  	@if($user->is_admin)
					  <a href="{{ route('user.posts', $user->id) }}" class="list-group-item {{ active('user/'.$user->id.'/posts') }}">
					  	<i class="fa fa-edit"></i> Posts
				  	</a>
				  	@endif
		  		  <a href="{{ route('user.comments', $user->id) }}" class="list-group-item {{ active('user/'.$user->id.'/comments') }}">
		  		  	<i class="fa fa-comments"></i> Comments
		  	  	</a>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			@yield('user-content')
		</div>
	</div>

</div>

@endsection