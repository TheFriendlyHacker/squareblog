@extends('user.user')
@section('page-title', ' :: '.$user->name.'\'s Posts')

@section('user-content')

	@include('components.success-alert')
	@include('components.error-alert')

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-edit"></i> {{ $user->first_name }}'s Posts <strong>({{ $user->posts->count() }})</strong></h3>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th><strong>Title</strong></th>
							<th><strong>Preview</strong></th>
							<th><strong>Date</strong></th>
						</tr>
					</thead>
					<tbody>
						@forelse($posts as $post)
						<tr>
							<td class="pd-t-14 pd-b-14">
								<a href="{{ route('post.show', $post->id) }}">{{ str_limit($post->title, 64) }}</a>
							</td>
							<td class="pd-t-14 pd-b-14">
								{{ str_limit($post->body, 100) }}
							</td>
							<td class="pd-t-14 pd-b-14">
								{{ $post->created_at->format('M d, Y') }}
							</td>
						</tr>
						@empty
						<tr>
							<td class="pd-t-30 pd-b-30 text-center">
								<strong><em>You have not created any blog posts</em></strong>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
			<div class="text-center">
				{{ $posts->links() }}
			</div>
		</div>
	</div>

@endsection