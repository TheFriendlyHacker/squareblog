@extends('user.user')
@section('page-title', ' :: '.$user->name.'\'s Profile')

@section('user-content')

	@include('components.success-alert')
	@include('components.error-alert')

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-user"></i> Profile</h3>
		</div>
		<div class="panel-body">
			<h3>{{ $user->name }}</h3>
			<p>{{ $user->about }}</p>
		</div>
	</div>

@endsection