<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ settings('app_name') }} @yield('page-title', '')</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    @include('partials.variables')
    <link rel="stylesheet" href="{{ asset(themes(settings('app_theme'))) }}">
    <link rel="stylesheet" href="{{ mix('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    @stack('stylesheets')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    @include('partials.navbar')

    <div id="app">

        @yield('content')

        <div class="container">
          @include('partials.footer')
        </div>

      </div>
      <!-- /.container -->
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
    @stack('scripts')
  </body>
</html>