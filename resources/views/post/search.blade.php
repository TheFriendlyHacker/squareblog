@extends('layouts.app')
@section('page-title', ' :: Search Results')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">
        <i class="fa fa-search"></i> Blog Search <small>({{ $posts->count() }} results)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li class="active">Search</li>
      </ol>
    </div>
  </div>
  <!-- /.row -->

  <!-- New search... -->
  @if(!$posts->isEmpty())
  <div class="row">
  	<div class="col-md-12">
      <div class="well">
        <form action="{{ route('post.search') }}" method="GET">
	        <div class="input-group">
	          <input type="text" name="q" class="form-control input-lg" placeholder="New Search">
	          <span class="input-group-btn">
		          <button class="btn btn-default btn-lg" type="submit"><i class="fa fa-search"></i></button>
	          </span>
	        </div>
        </form>
      </div>
  	</div>
  </div>

  <hr>
  @endif

	<!-- Display all of the blog posts... -->
  @forelse($posts as $post)
  @component('components.posts.blog-post-preview', ['post' => $post]) @endcomponent
	@empty
	<!-- If no search results were found... -->
	<div class="col-md-8 col-md-offset-2 text-center">
		<h3>No search results for "{{ $search }}"</h3>
		<div class="well">
	    <form action="{{ route('post.search') }}" method="GET">
	      <div class="input-group">
	        <input type="text" name="q" class="form-control input-lg" placeholder="Try another search">
	        <span class="input-group-btn">
	          <button class="btn btn-default btn-lg" type="submit"><i class="fa fa-search"></i></button>
	        </span>
	      </div>
	    </form>
		</div>
	</div>

  @endforelse
  <!-- /.row -->

  <hr>

  <!-- Pager -->
  <div class="row text-center">
    {{ $posts->links() }}
  </div>
  <!-- /.row -->
  <hr>
</div>
@endsection