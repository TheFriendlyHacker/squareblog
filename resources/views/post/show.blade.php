@extends('layouts.app')
@section('page-title', $post->title)

@section('content')
<div class="container">
  <!-- Page Heading/Breadcrumbs -->
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">
      	{{ $post->title }}
	      <br>
        <small>by <a href="{{ route('user.profile', $post->user->id) }}">{{ $post->user->name }}</a></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a>
        </li>
        <li class="active">Blog Post</li>
      </ol>
    </div>
  </div>


  <div class="row">
  	@include('components.success-alert')
  </div>


  <div class="row">
    <!-- Blog Post Content Column -->
    <div class="col-md-8">
      <div class="blog-post">
        <!-- Admin Options -->
        @if(auth()->id() === $post->user->id)
        <div class="panel panel-default">
          <div class="panel-body">
            <a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
            <form action="{{ route('post.destroy', $post->id) }}" method="POST" class="swal"
                  style="display: inline-block;"
                  data-swal-title="Are You Sure?"
                  data-swal-text="This cannot be undone.">
              {!! method_field('DELETE') !!}
              {!! csrf_field() !!}
              <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
            </form>
          </div>
        </div>
        @endif

        <hr>
        <!-- Date/Time -->
        <p><i class="fa fa-clock-o"></i> Posted on {{ $post->created_at->format('M d, Y') }}</p>
        <hr>
        <!-- Preview Image -->
        @if(isset($post->title_image))
        <div class="text-center">
          <img class="img-responsive blog-title-image" src="{{ asset($post->title_image) }}" alt="{{ $post->title }}" height="250">
          <hr>
        </div>
        @endif
        <!-- Post Content -->
        <div class="post-body">
          @markdown($post->body)
        </div>
        <hr>
      </div>


      <!-- Blog Comments -->
      @if(settings('allow_comments'))
      <!-- Comments Form -->
    	@include('post.show.comments.create')
      <hr>
      <h3>Comments ({{ $post->comments->count() }})</h3><br>
      <!-- Blog Comments -->
			@include('post.show.comments.comments')
      @endif

    </div>

		<!-- Sidebar -->
    <div class="col-md-4">

      <!-- Blog Search -->
      @include('post.show.blog-search')

      <!-- Recent Blog Posts -->
      @include('post.show.recent-posts')

    </div>
  </div>
</div>
@endsection