<div class="well">
  <h4>Blog Search</h4>
  <form action="{{ route('post.search') }}" method="GET">
    <div class="input-group">
      <input type="text" name="q" class="form-control">
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
      </span>
    </div>
  </form>
</div>