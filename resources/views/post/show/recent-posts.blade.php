<div class="well">
  <h4>Recent Blog Posts</h4>
  <div class="row">
    <div class="col-lg-12">
      <ul class="list-unstyled">
        @foreach($recent_posts as $rPost)
        <li>
          <i class="fa fa-angle-double-right"></i> <a href="{{ route('post.show', $rPost->id) }}">{{ str_limit($rPost->title, 50) }}</a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
  <!-- /.row -->
</div>