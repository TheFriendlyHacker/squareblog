@if(auth()->check())
<div class="well">
  <h4>Leave a Comment:</h4>
  <form role="form" action="{{ route('comments.store', $post->id) }}" method="POST">
    {!! csrf_field() !!}
    <div class="form-group">
      <textarea class="form-control" name="body" id="body" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

@else

<div class="well">
  <h4>
  	<a href="{{ route('login') }}">Log In</a> to Leave a Comment
	</h4>
</div>
@endif