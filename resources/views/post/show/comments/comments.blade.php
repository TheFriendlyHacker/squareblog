@foreach($comments as $comment)
<div class="media">
  <a class="pull-left" href="#">
  <img class="media-object" src="http://placehold.it/64x64" alt="">
  </a>
  <div class="media-body">
    <h4 class="media-heading"><a href="{{ route('user.profile', $comment->user->id) }}">{{ $comment->user->name }}</a>
      <small>{{ $comment->created_at->format('M d, Y') }}</small>
      @if(auth()->id() === $comment->user->id)
      <form action="{{ route('comments.destroy', [$post->id, $comment->id]) }}" method="POST"
            class="swal"
            style="display: inline-block;"
            data-swal-title="Are You Sure?"
            data-swal-text="This cannot be undone.">
        {!! csrf_field() !!}
        {!! method_field('DELETE') !!}
        &nbsp;<button class="btn btn-sm btn-link" type="submit"><span class="text-danger">delete</span></button>
      </form>
      @endif
    </h4>
    {{ $comment->body }}
  </div>
</div>
<hr>
@endforeach

<!-- Pagination Buttons -->
{{-- <div class="text-center">
	{{ $comments->links() }}
</div> --}}