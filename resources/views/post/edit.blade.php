@extends('layouts.app')
@section('page-title', ' :: Edit Post')

@push('stylesheets')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
@endpush
@push('scripts')
<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
@endpush

@section('content')
<div class="container">

	<div class="row">
	  <div class="col-lg-12">
	    <h1 class="page-header">
	      <i class="fa fa-edit"></i> Edit Post
	    </h1>
	    <ol class="breadcrumb">
	      <li><a href="{{ route('home') }}">Home</a></li>
	      <li class="active">Edit Post</li>
	    </ol>
	  </div>
	</div>

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			@include('components.success-alert')
			@include('components.error-alert')
			@include('components.form-errors-alert')

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil"></i> Edit Post</h3>
				</div>
				<div class="panel-body">
					<form action="{{ route('post.update', $post->id) }}" method="POST" role="form" enctype="multipart/form-data">
						{!! method_field('PUT') !!}
						{!! csrf_field() !!}

						<p><strong>Link to Post:</strong> <a href="{{ route('post.show', $post->id) }}">{{ route('post.show', $post->id) }}</a></p>

						<!-- Post Title -->
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
					    <input id="title" type="text" class="form-control input-lg" name="title" value="{{ old('title', $post->title) }}" placeholder="Your Post's Title" autofocus>
					    @if ($errors->has('title'))
					    <span class="help-block">
					    <strong>{{ $errors->first('title') }}</strong>
					    </span>
					    @endif
						</div>

						<!-- Post Body -->
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
							<textarea name="body" id="body" rows="20" class="mde">{{ old('body', $post->body) }}</textarea>
					    @if ($errors->has('title'))
					    <span class="help-block">
					    <strong>{{ $errors->first('title') }}</strong>
					    </span>
					    @endif
						</div>

						<!-- Thumbnail Image -->
						<div class="form-group">
							<label for="thunbnail" class="control-label">
								Thumbnail Image
								<i class="fa fa-question-circle" data-toggle="tooltip" title="This is the preview image that will show up on the home page."></i>
							</label>
							<input type="file" class="form-control" name="thumbnail" id="thunbnail">
							<span class="help-block"><strong><i class="fa fa-exclamation-triangle"></i> Image must be at least 450px x 225px</strong></span>

							<div class="text-center">
								<img src="{{ asset($post->thumbnail) }}" alt="Thumbnail Image" class="img-responsive" style="max-height: 300px;">
							</div>
						</div>

						<!-- Title Image -->
						<div class="form-group">
							<label for="title_image" class="control-label">
								Title Image (optional)
								<i class="fa fa-question-circle" data-toggle="tooltip" title="This is an optional image that will be displayed at the top of your blog post."></i>
							</label>
							<input type="file" class="form-control" name="title_image" id="title_image">
							<span class="help-block"><strong><i class="fa fa-exclamation-triangle"></i> Image must be at least 750px x 250px</strong></span>

							@if(isset($post->title_image))
							<div class="text-center">
								<img src="{{ asset($post->title_image) }}" alt="Thumbnail Image" class="img-responsive" style="max-height: 300px;">
							</div>
							@else
							<strong><em>No Title Image Set</em></strong>
							@endif
						</div>

						<!-- Publish Option -->
						<div class="form-group">
							<input type="hidden" name="published" value="0">
							<div class="checkbox">
							  <label><input type="checkbox" name="published" id="published" value="1" @if($post->published) checked @endif>
							  	Publish Post? <i class="fa fa-question-circle" data-toggle="tooltip" title="Should this post be visible to the public?"></i>
						  	</label>
							</div>
						</div>

						<div class="form-group">
							<button class="btn btn-primary" type="submit"><i class="fa fa-check-circle"></i> Save Changes</button>
							<a href="{{ route('post.edit', $post->id) }}" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection