<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostsController@index')->name('home');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('/contact', 'ContactController@sendContactRequest');


Route::group(['prefix' => 'post'], function() {
	// Post Routes
	Route::get('/search', 'PostsController@search')->name('post.search');
	Route::get('/{id}/edit', 'PostsController@edit')->name('post.edit');
	Route::get('/create', 'PostsController@create')->name('post.create');
	Route::post('/', 'PostsController@store')->name('post.store');
	Route::put('/{id}', 'PostsController@update')->name('post.update');
	Route::get('/{id}', 'PostsController@show')->name('post.show');
	Route::delete('/{id}', 'PostsController@destroy')->name('post.destroy');

	// Comment Routes
	Route::group(['prefix' => '/{postId}/comments'], function() {
		Route::post('/', 'CommentsController@store')->name('comments.store');
		Route::delete('/{id}', 'CommentsController@destroy')->name('comments.destroy');
	});
});

/**
 * User Profile Routes
 */
Route::group(['prefix' => 'user', 'namespace' => 'Users'], function() {
	Route::get('/{id}', 'ProfileController@show')->name('user.profile');
	Route::get('/{id}/comments', 'CommentsController@index')->name('user.comments');
	Route::get('/{id}/posts', 'PostsController@index')->name('user.posts');
});

/**
 * Current Authenticated User Profile Routes
 */
Route::group(['prefix' => 'me', 'namespace' => 'Me'], function() {
	// Profile
	Route::get('/', 'ProfileController@edit')->name('me.profile');
	Route::put('/', 'ProfileController@update')->name('me.profile.update');
	Route::put('/about', 'ProfileController@updateAbout')->name('me.profile.about.update');
	Route::put('/picute', 'ProfileController@updatePicture')->name('me.profile.picture.update');
	// Security
	Route::get('/security', 'PasswordController@edit')->name('me.security');
	Route::put('/security', 'PasswordController@update');
	// Comments
	Route::get('/comments', 'CommentsController@index')->name('me.comments');
	// Posts
	Route::get('/posts', 'PostsController@index')->name('me.posts');
});


/**
 * Admin Routes - only accessible by admins.
 */
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => [ 'admin' ]], function() {
	Route::get('/', 'SettingsController@index')->name('admin.settings');
	Route::put('/', 'SettingsController@update');
	Route::get('/users', 'UsersController@index')->name('admin.users');
	Route::get('/about', 'AboutController@index')->name('admin.about');
	Route::put('/about', 'AboutController@update');
	Route::get('/contact', 'ContactController@index')->name('admin.contact');
	Route::put('/contact', 'ContactController@update');
});

Auth::routes();

if(env('APP_ENV') !== 'production') {
	Route::get('/test', 'TestController@test');
}
