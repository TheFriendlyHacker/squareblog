let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.js('resources/assets/js/app.js', 'public/js/app.js').version()
  .sass('resources/assets/sass/app.scss', 'public/css/app.css').version()
  .styles([
  	'resources/assets/css/sweetalert.css'
  ], 'public/css/vendor.css').version()
  .copy('resources/assets/css/themes', 'public/css/themes');
