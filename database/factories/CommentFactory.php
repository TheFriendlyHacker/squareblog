<?php
$factory->define(App\Comment::class, function (Faker\Generator $faker) {
  return [
    'body' => $faker->realText(500),
    'post_id' => \App\Post::get()->random()->id,
    'user_id' => \App\User::get()->random()->id,
  ];
});
