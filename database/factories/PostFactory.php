<?php
$factory->define(App\Post::class, function (Faker\Generator $faker) {
  return [
    'title' => $faker->sentence(3, 6),
    'body' => $faker->realText(1000),
    'thumbnail' => 'generic-stock-thumbnail.jpg',
    'user_id' => \App\User::admins()->get()->random()->id
  ];
});
