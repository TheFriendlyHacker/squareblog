<?php
$factory->define(App\User::class, function (Faker\Generator $faker) {
  return [
    'first_name' => $faker->firstName,
    'last_name' => $faker->lastName,
    'email' => $faker->unique()->safeEmail,
    'about' => $faker->paragraph(mt_rand(5,20)),
    'password' => bcrypt('secret')
  ];
});

$factory->state(App\User::class, 'admin', function ($faker) {
  return [
  	'first_name' => 'Admin',
  	'last_name' => 'User',
  	'email' => 'admin@example.com',
    'is_admin' => 1,
  ];
});
