# SquareBlog #

SquareBlog is a simple but flexible starter application for a blog website. Powered by Laravel and Vue.js.


### What Does SquareBlog Do? ###
SquareBlog provides all of the core scaffolding you need to get a basic blog site up and running in no time. Simply install it, create an admin account and you're ready to start blogging!

SquareBlog ships with a basic bootstrap design, making it easy for you to restyle it to your own liking.

### Features ###
* Create blog posts (full markdown support, of course)
* Allow your followers to comment on your posts
* Searchable user profiles
* Full-featured admin panel, allowing you to further customize your site
* Simple, [Bootstrap](https://getbootstrap.com)-based design
* Easy to re-style/redesign to your liking
* Email notifications

### Dependencies ###
Required Dependencies:

* [Laravel 5.4](https://laravel.com)
* PHP >= 7.0 (will eventually support 5.6)
* [Composer](https://getcomposer.org)

Recommended/Required for Development:

* [NPM](https://npmjs.com)


### How Do I Install It? ###
You may either install SquareBlog via git or via direct download. For both installation methods, you should have console/SSH access to your server/host.

After you have installed SquareBlog, follow these steps to get up and running:

1. `cd` into the root directory of your SquareBlog installation
2. run `chmod -R 777 boostrap app storage`
3. run `composer install`
4. rename the `.env.example` file to `.env`
5. Open your new `.env` file and fill in your database credentials.
6. run `php artisan migrate` to set up your database.
7. run `php artisan storage:link` to allow users to upload images.
9. run `php artisan squareblog:install`, and follow the instructions
9. You're ready to start blogging!



### Who do I talk to? ###

* email: christian@redsquareweb.com