<?php
namespace App\Repositories;

use App\Contracts\Repositories\CommentRepository as CommentRepositoryContract;
use App\{ User, Comment, Post };

class CommentRepository implements CommentRepositoryContract
{
  /**
   * Retrieve all of the comments created by the given user.
   *
   * @param  User   $user
   * @return Collection
   */
  public function forUser(User $user)
  {
  	return $user->comments()->get();
  }

  /**
   * Retrieve a comment by its id.
   *
   * @param  int $id
   * @return Comment
   */
  public function byId($id)
  {
  	return Comment::with('post', 'user')->findOrFail($id);
  }

  /**
   * Create a new comment in storage.
   *
   * @param  User   $user
   * @param Post $post
   * @param  array  $data
   * @return Comment
   */
  public function create(User $user, Post $post, array $data)
  {
  	$comment = Comment::create([
  		'body' => $data['body'],
  		'user_id' => $user->id,
  		'post_id' => $post->id
  	]);

  	return $this->byId($comment->id);
  }
}
