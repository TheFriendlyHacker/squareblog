<?php
namespace App\Repositories;

use App\Contracts\Repositories\PostRepository as PostRepositoryContract;
use Illuminate\Http\UploadedFile;
use App\{ User, Post };

class PostRepository implements PostRepositoryContract
{
	/**
	 * Retrieve ALL posts.
	 * @return Collection
	 */
	public function all($paginate = null, $includeUnpublished = false)
	{
		$query = Post::with('user', 'comments')->orderBy('created_at', 'desc');

		if(!$includeUnpublished) {
			$query = $query->where('published', 1);
		}

		return (isset($paginate) && is_integer($paginate))
						? $query->paginate($paginate)
						: $query->get();
	}

	/**
	 * Retrieve all of the posts created by the given user.
	 *
	 * @param  User   $user
	 * @return Collection
	 */
	public function forUser(User $user)
	{
		return $user->posts()->get();
	}

	/**
	 * Retrieve a post by its id.
	 *
	 * @param  int $id
	 * @return Post
	 */
	public function byId($id)
	{
		return Post::with('comments', 'user')->findOrFail($id);
	}

	/**
	 * Retrieve all posts with a title similar to the given one.
	 *
	 * @param  string $title
	 * @return Collection
	 */
	public function byTitle($title, $paginate = null)
	{
		$query = Post::with('user', 'comments')
						->orderBy('created_at', 'desc')
						->where('title', 'like', '%'.$title.'%');

		return (isset($paginate) && is_integer($paginate))
						? $query->paginate($paginate)
						: $query->get();
	}

	/**
	 * Create a new post in storage.
	 *
	 * @param  User   $user
	 * @param  array  $data includes the posts title+body+options as well as any images.
	 * @return Post
	 */
	public function create(User $user, array $data)
	{
		$thumbnailPath = (isset($data['thumbnail']))
											? $this->storeBlogImage($data['thumbnail'])
											: null;
		$titleImagePath = (isset($data['title_image']))
											? $this->storeBlogImage($data['title_image'])
											: null;

		$post = Post::create([
			'title' => $data['title'],
			'body' => $data['body'],
			'user_id' => $user->id,
			'published' => $data['published'],
			'thumbnail' => $thumbnailPath,
			'title_image' => $titleImagePath
		]);

		return $this->byId($post->id);
	}

	/**
	 * Update a post in storage.
	 *
	 * @param  Post   $post
	 * @param  array  $data
	 * @return Post
	 */
	public function update(Post $post, array $data)
	{
		$post->title = $data['title'];
		$post->body = $data['body'];
		$post->published = $data['published'];

		if(isset($data['thumbnail'])) {
			$thumbnailPath = $this->storeBlogImage($data['thumbnail']);
			$post->thumbnail = $thumbnailPath;
		}

		if(isset($data['title_image'])) {
			$titleImagePath = $this->storeBlogImage($data['title_image']);
			$post->title_image = $titleImagePath;
		}

		$post->save();

		return $this->byId($post->id);
	}

	/**
	 * Store the given thumnail image.
	 *
	 * @param  UploadedFile $file
	 * @return string  the name of the newly stored file.
	 */
	protected function storeBlogImage(UploadedFile $file)
	{
		$path = $file->store(config('squareblog.blog_file_location'));
		$pathArray = explode('/', $path);
		return end($pathArray);
	}
}
