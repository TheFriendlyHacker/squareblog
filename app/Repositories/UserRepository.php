<?php
namespace App\Repositories;

use App\Contracts\Repositories\UserRepository as UserRepositoryContract;
use App\User;
use Auth;

class UserRepository implements UserRepositoryContract
{
	/**
	 * Retrieve the current authenticated user.
	 *
	 * @return User or null
	 */
	public function currentUser()
	{
		return User::with('posts', 'comments')->find(Auth::id());
	}

	/**
	 * Retrieve all users.
	 *
	 * @param  int or null $paginate
	 * @return Collection
	 */
	public function all($paginate = null)
	{
		$query = User::with('posts', 'comments')->orderBy('created_at', 'desc');

		return (isset($paginate) && is_integer($paginate))
						? $query->paginate($paginate)
						: $query->get();
	}

	/**
	 * Retrieve a user by its id.
	 *
	 * @param  int $id
	 * @return User
	 */
	public function byId($id)
	{
		return User::with('posts', 'comments')->findOrFail($id);
	}

	/**
	 * Find all users with similar names/emails to the given search query.
	 *
	 * @param  string $query
	 * @param int or null $paginate
	 * @return Collection
	 */
	public function bySearchQuery($query, $paginate = null)
	{
		$query = User::with('posts', 'comments')
						->whereRaw("concat(first_name, ' ', last_name) like '%$query%' ")
						->orWhere('email', 'like', '%'.$query.'%');

		return (isset($paginate) && is_integer($paginate))
						? $query->paginate($paginate)
						: $query->get();
	}
}
