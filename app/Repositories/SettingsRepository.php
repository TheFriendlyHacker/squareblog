<?php
namespace App\Repositories;

use App\Contracts\Repositories\SettingsRepository as SettingsRepositoryContract;
use App\User;
use Settings;

class SettingsRepository implements SettingsRepositoryContract
{
	/**
	 * Retrieve all app settings.
	 *
	 * @return StdObject
	 */
	public function all()
	{
		return (object) Settings::all();
	}

	/**
	 * Get a specified setting.
	 *
	 * @param  string or array $key
	 * @return mixed
	 */
	public function get($key)
	{
		$settings;
		if(is_array($key)) {
			$settings = [];
			foreach($key as $k) {
				$settings[$k] = Settings::get($k, null);
			}
		} else {
			$settings = Settings::get($key, null);
		}

		return (is_array($settings))
						? (object) $settings
						: $settings;
	}

	/**
	 * Set a specified setting value (if it exists).
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return  $this
	 */
	public function set($key, $value)
	{
		$settings = Settings::all();
		if(array_key_exists($key, $settings)) {
			Settings::set($key, $value);
			Settings::save();
		}

		return $this;
	}

	/**
	 * Update app settings.
	 *
	 * @param  array  $data ['setting_name' => $newValue]
	 * @return StdObject
	 */
	public function update(array $data)
	{
		$currentSettings = Settings::all();

		foreach($data as $key => $value) {
			if(array_key_exists($key, $currentSettings)) {
				Settings::set($key, $value);
			}
		}

		Settings::save();

		return $this->all();
	}

	/**
	 * Get or set the name of the application.
	 *
	 * @param  string or null $name null = return the current name of the app.
	 * @return string
	 */
	public function appName($name = null)
	{
		if(isset($name)) {
			Settings::set('app_name', $name);
			Settings::save();
			return Settings::get('app_name', 'SquareBlog');
		}

		return Settings::get('app_name', 'SquareBlog');
	}

	/**
	 * Get or set the title that appears on the home page.
	 *
	 * @param  string or null $title null = return the current title
	 * @return string
	 */
	public function homeTitle($title = null)
	{
		if(isset($name)) {
			Settings::set('home_title', $title);
			Settings::save();
			return Settings::get('home_title', 'Welcome to SquareBlog');
		}

		return Settings::get('home_title', 'Welcome to SquareBlog');
	}

	/**
	 * Get or set whether or not the "about" page should be visible.
	 *
	 * @param  boolean or null $show null = return current setting for whether the page should be visible
	 * @return boolean
	 */
	public function showAboutPage($show = null)
	{
		if(isset($name)) {
			Settings::set('show_about_page', $show);
			Settings::save();
			return Settings::get('show_about_page', false);
		}

		return Settings::get('show_about_page', false);
	}

	/**
	 * Get or set the "about us" description, for the about page.
	 *
	 * @param  string or null $title null = return the current "about us" description
	 * @return string
	 */
	public function about($about = null)
	{
		if(isset($name)) {
			Settings::set('about', $about);
			Settings::save();
			return Settings::get('about', "");
		}

		return Settings::get('about', ".");
	}
}
