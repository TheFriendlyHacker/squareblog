<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class MakeTraitCommand extends GeneratorCommand
{
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'make:trait';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create a new trait.';

  /**
   * The type of class being generated.
   *
   * @var string
   */
  protected $type = 'Trait';

  /**
   * Execute the console command.
   *
   * @return void
   */
  public function fire()
  {
    if (parent::fire() === false) {
      return;
    }
  }

  /**
   * Get the stub file for the generator.
   *
   * @return string
   */
  protected function getStub()
  {
    return __DIR__.'/stubs/trait.stub';
  }

  /**
   * Get the default namespace for the class.
   *
   * @param  string  $rootNamespace
   * @return string
   */
  protected function getDefaultNamespace($rootNamespace)
  {
    return $rootNamespace;
  }

  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    return [
      //
    ];
  }
}
