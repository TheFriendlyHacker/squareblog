<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class MakeContractCommand extends GeneratorCommand
{
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'make:contract';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Generate a new contract';

  /**
   * The type of class being generated.
   *
   * @var string
   */
  protected $type = 'Contract';

  /**
   * Execute the console command.
   *
   * @return void
   */
  public function fire()
  {
    parent::fire();
  }

  /**
   * Build the class with the given name.
   *
   * @param  string  $name
   * @return string
   */
  protected function buildClass($name)
  {
    $stub = parent::buildClass($name);

    return $stub;
  }

  /**
   * Get the stub file for the generator.
   *
   * @return string
   */
  protected function getStub()
  {
    return __DIR__.'/stubs/contract.stub';
  }

  /**
   * Determine if the class already exists.
   *
   * @param  string  $rawName
   * @return bool
   */
  protected function alreadyExists($rawName)
  {
    return class_exists($rawName);
  }

  /**
   * Get the default namespace for the class.
   *
   * @param  string  $rootNamespace
   * @return string
   */
  protected function getDefaultNamespace($rootNamespace)
  {
    switch($this->option('type')) {
      case 'repository':
        return $rootNamespace.'\Contracts\Repositories';
      break;
      default:
        return $rootNamespace.'\Contracts';
      break;
    }
  }

  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    return [
      ['type', 'c', InputOption::VALUE_OPTIONAL, 'The type of class that will implement the contract (e.g. "repository", "model").'],
    ];
  }
}
