<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class MakeClassCommand extends GeneratorCommand
{
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'make:class';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Generate a new class.';

  /**
   * The type of class being generated.
   *
   * @var string
   */
  protected $type = 'Class';

  /**
   * Execute the console command.
   *
   * @return void
   */
  public function fire()
  {
    // Generate the contract for the repository
    if($this->option('contract'))
    {
      $this->call('make:contract', [
        'name' => "{$this->option('contract')}"
      ]);
    }

    parent::fire();
  }

  /**
   * Build the class with the given name.
   *
   * @param  string  $name
   * @return string
   */
  protected function buildClass($name)
  {
    $stub = parent::buildClass($name);

    if($this->option('contract'))
    {
      $contract = $contract = $this->laravel->getNamespace().'Contracts\\'.$this->option('contract');;

      $stub = str_replace(
        'DummyContract', class_basename($contract), $stub
      );

      $stub = str_replace(
        'DummyAliasContract', class_basename($contract).'Contract', $stub
      );

      $stub = str_replace(
        'DummyFullContract', $contract, $stub
      );
    }

    return $stub;
  }

  /**
   * Get the stub file for the generator.
   *
   * @return string
   */
  protected function getStub()
  {
    return ($this->option('contract'))
            ? __DIR__.'/stubs/class-with-contract.stub'
            : __DIR__.'/stubs/class.stub';
  }

  /**
   * Determine if the class already exists.
   *
   * @param  string  $rawName
   * @return bool
   */
  protected function alreadyExists($rawName)
  {
    return class_exists($rawName);
  }

  /**
   * Get the default namespace for the class.
   *
   * @param  string  $rootNamespace
   * @return string
   */
  protected function getDefaultNamespace($rootNamespace)
  {
    return $rootNamespace;
  }

  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    return [
      ['contract', 'c', InputOption::VALUE_OPTIONAL, 'The contract for the repository to implement.'],
    ];
  }
}
