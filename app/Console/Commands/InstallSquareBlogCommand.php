<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InstallSquareBlogCommand extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'squareblog:install';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Walks you throught he process of setting up your SquareBlog installation.';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->greet();
  }

  protected function greet()
  {
    $this->info('Welcome to SquareBlog!');
    $this->line('');
  }
}
