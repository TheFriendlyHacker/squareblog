<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class MakeRepositoryCommand extends GeneratorCommand
{
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'make:repository';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Generate a new repository';

  /**
   * The type of class being generated.
   *
   * @var string
   */
  protected $type = 'Repository';

  /**
   * Execute the console command.
   *
   * @return void
   */
  public function fire()
  {
    // Generate the contract for the repository
    $contract = ($this->option('contract'))
                ? $this->option('contract')
                : Str::studly(class_basename($this->argument('name')));

    $this->call('make:contract', [
        'name' => "{$contract}",
        '--type' => 'repository'
    ]);

    parent::fire();
  }

  /**
   * Build the class with the given name.
   *
   * @param  string  $name
   * @return string
   */
  protected function buildClass($name)
  {
    $stub = parent::buildClass($name);

    if($this->option('contract')) {
      $contract = $this->option('contract');
    } else {
      $contract = $this->laravel->getNamespace().'Contracts\\Repositories\\'.$this->argument('name');
    }

    $stub = str_replace(
      'DummyContract', class_basename($contract), $stub
    );

    $stub = str_replace(
      'DummyAliasContract', class_basename($contract).'Contract', $stub
    );

    $stub = str_replace(
      'DummyFullContract', $contract, $stub
    );

    return $stub;
  }

  /**
   * Get the stub file for the generator.
   *
   * @return string
   */
  protected function getStub()
  {
    return __DIR__.'/stubs/repository.stub';
  }

  /**
   * Determine if the class already exists.
   *
   * @param  string  $rawName
   * @return bool
   */
  protected function alreadyExists($rawName)
  {
    return class_exists($rawName);
  }

  /**
   * Get the default namespace for the class.
   *
   * @param  string  $rootNamespace
   * @return string
   */
  protected function getDefaultNamespace($rootNamespace)
  {
    return $rootNamespace.'\Repositories';
  }

  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    return [
      ['contract', 'c', InputOption::VALUE_OPTIONAL, 'The contract for the repository to implement.'],
    ];
  }
}
