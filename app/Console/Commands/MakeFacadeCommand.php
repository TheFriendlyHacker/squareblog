<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class MakeFacadeCommand extends GeneratorCommand
{
/**
 * The console command name.
 *
 * @var string
 */
protected $name = 'make:facade';

/**
 * The console command description.
 *
 * @var string
 */
protected $description = 'Generate a new facade.';

/**
 * The type of class being generated.
 *
 * @var string
 */
protected $type = 'Facade';

/**
 * Execute the console command.
 *
 * @return void
 */
public function fire()
{
  parent::fire();
}

/**
 * Build the class with the given name.
 *
 * @param  string  $name
 * @return string
 */
protected function buildClass($name)
{
  $stub = parent::buildClass($name);

  $stub = str_replace(
    'DummyFacadeAccessor', $this->option('accessor'), $stub
  );

  return $stub;
}

/**
 * Get the stub file for the generator.
 *
 * @return string
 */
protected function getStub()
{
  return __DIR__.'/stubs/facade.stub';
}

/**
 * Get the default namespace for the class.
 *
 * @param  string  $rootNamespace
 * @return string
 */
protected function getDefaultNamespace($rootNamespace)
{
  return $rootNamespace. '\Facades';
}

/**
 * Get the console command options.
 *
 * @return array
 */
protected function getOptions()
{
  return [
    ['accessor', 'a', InputOption::VALUE_REQUIRED, 'The facade accessor.'],
  ];
}
}
