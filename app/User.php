<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class User extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   */
  protected $fillable = [
    'first_name', 'last_name', 'email', 'password', 'about',
  ];
  /**
   * The attributes that should be hidden for arrays.
   */
  protected $hidden = [
    'password', 'remember_token',
  ];
  /**
   * Date fields (returns a Carbon\Carbon instance).
   */
  protected $dates = ['created_at', 'updated_at'];
  /**
   * Attribute casts.
   */
  protected $casts = [
    'is_admin' => 'boolean'
  ];

  /**
   * Retrieve all of the posts the user has posted.
   *
   * @return Collection
   */
  public function posts()
  {
    return $this->hasMany(Post::class);
  }

  /**
   * Retrieve all of the comments the user has posted.
   *
   * @return Collection
   */
  public function comments()
  {
    return $this->hasMany(Comment::class);
  }

  /**
   * Special getter for the user's full name.
   *
   * @return string
   */
  public function getNameAttribute()
  {
    return $this->first_name . ' ' . $this->last_name;
  }

  /**
   * Special setter that ensures the new password is always hashed.
   *
   * @param string $value
   */
  public function setPasswordAttribute($value)
  {
    if(Hash::needsRehash($value)) {
      $this->attributes['password'] = bcrypt($value);
    } else {
      $this->attributes['password'] = $value;
    }
  }

  /**
   * Scope a query to only include admin users.
   *
   * @param  Illuminate\Database\Eloquent\Builder $query
   * @return Illuminate\Database\Eloquent\Builder
   */
  public function scopeAdmins($query)
  {
    return $query->where('is_admin', 1);
  }
}
