<?php
/**
 * Return the client's real IP, in case CloudFlare is active.
 *
 * @return string
 */
function ip()
{
	return (array_key_exists('HTTP_CF_CONNECTING_IP', $_SERVER))
					? $_SERVER['HTTP_CF_CONNECTING_IP']
					: request()->ip();
}

/**
 * Use CloudFlare to get the client's country.
 * Since this relies on your DNS being routed through CloudFlare, it's not recommended that your
 * application relies on this method.
 *
 * @return string or null
 */
function client_country()
{
	return (array_key_exists('HTTP_CF_IPCOUNTRY', $_SERVER))
					? $_SERVER['HTTP_CF_IPCOUNTRY']
					: null;
}

/**
 * Return an 'active' class if the given uri matches the current uri.
 *
 * @param  string  $uri
 * @param  boolean $exact false = $path + wildcard
 * @param  string  $class
 * @return string
 */
function active($uri, $exact = false, $class = "active")
{
	if(empty($uri)) {
		return Request::is('/') ? $class : '';
	}
	$path = ($exact) ? $uri : $uri . '*';
  return Request::is($path) ? $class :  '';
}

/**
 * Return the applications morphMap for polymorphic relationships.
 * @return array
 */
function morphMap()
{
	return \Illuminate\Database\Eloquent\Relations\Relation::morphMap();
}

/**
 * Convert Markdown to HTML using the Parsedown library.
 * THIS DOES NOT SANITIZE THE HTML!
 * https://github.com/erusev/parsedown
 *
 * @param  string $markdown
 * @return string/html
 */
function parsedown($markdown)
{
	$parsedown = new \Parsedown();
	return $parsedown->text($markdown);
}

/**
 * Generate names for a group of resource routes, used for Route::resource()
 *
 * @param string $prefix  the prefix for each route name
 * @return array
 */
function resourceRouteNames($prefix)
{
	return [
		'index' => $prefix.'index', 'create' => $prefix.'create', 'store' => $prefix.'store',
		'show' => $prefix.'show', 'edit' => $prefix.'edit', 'update' => $prefix.'update', 'destroy' => $prefix.'destroy'
	];
}

/**
 * Explode the contents of a file, using newlines as the delimiter.
 *
 * @param  string $path
 * @return array
 */
function explodeFileByNewlines($path)
{
	return file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
}

/**
 * If the input is empty, return null. Else, return the input.
 *
 * @param  mixed $input
 * @return mixed
 */
function nullable($input)
{
	return (empty($input)) ? null : $input;
}

/**
 * Retrieve the path to our JSON data files.
 *
 * @param  String $append a path to append to the data path.
 * @return String
 */
function data_path($append = '')
{
	$append = (starts_with($append, '/')) ? $append : '/'.$append;
	return base_path(config('app.data_path').$append);
}

/**
 * Retrieve and parse the contents of a JSON file.
 *
 * @param  String $path
 * @param boolean $collection return a Collection, or a regular JSON response?
 * @return mixed
 */
function json_data($path, $collection = false)
{
	if(!\File::exists($path) || File::extension($path) !== 'json') {
		return null;
	}
	$data = json_decode(\File::get($path));
	return ($collection) ? collect($data) : $data;
}

/**
 * Get or set an app setting.
 *
 * @param  null or string $key   null = return new instance of \Labkod\LaravelConfig\LaravelConfig
 * @param  null or mixed $value null = retrieve value of $key. Else, $key will be set to this value
 * @return mixed
 */
function settings($key = null, $value = null)
{
	if(!isset($key)) {
		return app(\Labkod\LaravelConfig\LaravelConfig::class);
	}
	elseif(isset($key) && !isset($value)) {
		$setting = \Settings::get($key, null);
		return (is_array($setting)) ? (object) $setting : $setting;
	}
	elseif(array_key_exists($key, Settings::all())) {
		Settings::set($key, $value);
		$setting = \Settings::get($key, null);
		return (is_array($setting)) ? (object) $setting : $setting;
	}
	else {
		return null;
	}
}

/**
 * Retrieve a listing of all available themes for the app, or the file location for a specified theme.
 *
 * @param string or null $theme
 * @return array
 */
function themes($theme = null)
{
	$files = File::files(base_path('/resources/assets/css/themes'));
	$themes = [];
	foreach($files as $file) {
		$filePathArray = explode('/', $file);
		$fileName = end($filePathArray);
		array_push($themes, explode('.', $fileName)[0]);
	}

	if(isset($theme)) {
		if(!in_array($theme, $themes)) return null;
		return 'css/themes/'.$theme.'.css';
	}
	return $themes;
}

/**
 * If the given value is empty or null, return the given $default. Else, return the value.
 *
 * @param  mixed $value
 * @param  mixed $default
 * @return mixed
 */
function ifEmpty($value, $default = null)
{
	if(!isset($value) || empty($value)) {
		return $default;
	}
	return $value;
}

/**
 * If the given value equates to 'false', return the given $default. Else, return the value.
 *
 * @param  mixed $value
 * @param  mixed $default
 * @return mixed
 */
function ifFalse($value, $default = null)
{
	return (!$value) ? $default : $value;
}