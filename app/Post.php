<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  /**
   * Attributes that are not mass-assignable.
   */
  protected $guarded = [];
  /**
   * Date fields (returns a Carbon\Carbon instance).
   */
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];
  /**
   * Attribute casts.
   */
  protected $casts = [
    'published' => 'boolean'
  ];

	/**
	 * Retrieve the user who created the post.
	 *
	 * @return User
	 */
  public function user()
  {
  	return $this->belongsTo(User::class);
  }

  /**
   * Retrieve all comments on the post.
   *
   * @return Collection
   */
  public function comments()
  {
  	return $this->hasMany(Comment::class);
  }

  /**
   * Getter for the link to the post's tumbnail image. Returns a link to a generic placeholder if thumbnail is null.
   *
   * @param  string or null $value
   * @return string
   */
  public function getThumbnailAttribute($value)
  {
    if(isset($value)) {
      $symPathArray = explode('/', config('squareblog.blog_file_location'));
      $symPath = end($symPathArray);
      return 'storage/' . $symPath . '/' . $value;
    }

    return null;
  }

  /**
   * Getter for the link to the post's title image. Returns null if no title image is set.
   *
   * @param  string or null $value
   * @return string
   */
  public function getTitleImageAttribute($value)
  {
    if(isset($value)) {
      $symPathArray = explode('/', config('squareblog.blog_file_location'));
      $symPath = end($symPathArray);
      return 'storage/' . $symPath . '/' . $value;
    }

    return null;
  }
}
