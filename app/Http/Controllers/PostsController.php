<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Http\UploadedFile;

use App\Contracts\Repositories\{ PostRepository, UserRepository };

class PostsController extends Controller
{
  /**
   * UserRepository
   */
  protected $users;
  /**
   * PostRepository
   */
  protected $posts;

  /**
   * Class constructor.
   *
   * @param UserRepository $users
   * @param PostRepository $posts
   */
  public function __construct(UserRepository $users, PostRepository $posts)
  {
    $this->middleware('auth')->except('show', 'search', 'index');
    $this->users = $users;
    $this->posts = $posts;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $posts = $this->posts->all(10);

    return view('home', compact('posts'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $user = $this->users->currentUser();
    if(!$user->is_admin) return redirect('home');

    return view('post.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $user = $this->users->currentUser();
    if(!$user->is_admin) return redirect(route('home'));

    $this->validatePost($request);
    $this->validateThumbnailImage($request);
    $this->validateTitleImage($request);

    $post = $this->posts->create($user, $request->all());

    return redirect(route('post.show', $post->id))
                    ->with('success', trans('messages.posts.successfully-created'));
  }

  /**
   * Display the specified resource.
   *
   * @param Request $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
    $user = $this->users->currentUser();
    $post = $this->posts->byId($id);

    // make sure it's published, or that the owner/an admin is viewing it.
    if(!$post->published) {
      if($user->id !== $post->user_id && !$user->is_admin) {
        return redirect(route('home'));
      }
    }

    $comments = $post->comments()->orderBy('created_at', 'asc')->get();

    $recent_posts = $this->posts->all()->take(6);

    return view('post.show', compact('post', 'comments', 'recent_posts'));
  }

  /**
   * Display specified posts based on a given search query.
   *
   * @param  Request $request
   * @return \Illuminate\Http\Response
   */
  public function search(Request $request)
  {
    $search = $request->q;
    $posts = $this->posts->byTitle($search, 10);

    return view('post.search', compact('posts', 'search'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $user = $this->users->currentUser();
    if(!$user->is_admin) return redirect('home');

    $post = $user->posts()->findOrFail($id);

    return view('post.edit', compact('post'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $user = $this->users->currentUser();
    if(!$user->is_admin) return redirect('home');

    $post = $user->posts()->findOrFail($id);

    $this->validatePost($request);
    if($request->hasFile('thumbnail')) $this->validateThumbnailImage($request);
    if($request->hasFile('title_image')) $this->validateTitleImage($request);

    $post = $this->posts->update($post, $request->all());

    return redirect(route('post.edit', $post->id))
                  ->with('success', trans('messages.posts.successfully-updated'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Request $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {
    $user = $this->users->currentUser();
    $post = $user->posts()->with('comments')->findOrFail($id);

    $post->comments()->delete();
    $post->delete();

    return redirect(route('home'))
            ->with('success', trans('messages.posts.successfully-deleted'));
  }

  /**
   * Validate incoming data for creating a new post.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validatePost(Request $request)
  {
    $this->validate($request, [
      'title' => 'required|max:255',
      'body' => 'required',
      'published' => 'required|in:1,0',
    ]);
  }

  /**
   * Validate incoming thumbnail image.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validateThumbnailImage(Request $request)
  {
    $this->validate($request, [
      'thumbnail' => 'nullable|dimensions:min_height=225|mimes:jpg,jpeg,png,gif,bmp',
    ]);
  }

  /**
   * Validate an incoming title image.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validateTitleImage(Request $request)
  {
    $this->validate($request, [
      'title_image' => 'dimensions:min_height=250|mimes:jpg,jpeg,png,gif,bmp'
    ]);
  }
}
