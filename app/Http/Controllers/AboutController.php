<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\Repositories\SettingsRepository;

class AboutController extends Controller
{
  /**
   * SettingsRepository
   */
  protected $settings;

	/**
	 * Class constructor.
   *
   * @param SettingsRepository $settings
	 */
  public function __construct(SettingsRepository $settings)
  {
  	$this->settings = $settings;
  }

	/**
	 * Display the about page.
	 *
	 * @param  Request $request
	 * @return Illuminate\Http\Response
	 */
  public function index(Request $request)
  {
  	if(!$this->settings->get('show_about_page')) abort(404);
  	$about = $this->settings->get(['about_title', 'about_body', 'about_image']);

  	return view('about', compact('about'));
  }
}
