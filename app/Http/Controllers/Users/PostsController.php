<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Repositories\UserRepository;

class PostsController extends Controller
{
	/**
	 * UserRepository
	 */
	protected $users;

	/**
	 * Class constructor.
	 *
	 * @param UserRepository $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->users = $users;
	}

	/**
	 * List all of the current user's comments.
	 *
	 * @param  Request $request
	 * @param int $id
	 * @return Illuminate\Http\Response
	 */
	public function index(Request $request, $id)
	{
		$user = $this->users->byId($id);
		$posts = $user->posts()
										->orderBy('created_at', 'desc')
										->paginate(10);

		return view('user.posts', compact('posts', 'user'));
	}
}
