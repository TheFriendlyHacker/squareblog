<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Contracts\Repositories\UserRepository;

class ProfileController extends Controller
{
	/**
	 * UserRepository
	 */
	protected $users;

	/**
	 * Class constructor.
	 *
	 * @param UserRepository $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->users = $users;
	}

  /**
   * Show the current authenticated user's profile.
   *
   * @param  Request $request
   * @param int $id
   * @return Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
  	$user = $this->users->byId($id);

  	return view('user.profile', compact('user'));
  }
}
