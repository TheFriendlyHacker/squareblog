<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Repositories\UserRepository;

class CommentsController extends Controller
{
	/**
	 * UserRepository
	 */
	protected $users;

	/**
	 * Class constructor.
	 *
	 * @param UserRepository $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->users = $users;
	}

	/**
	 * List all of the current user's comments.
	 *
	 * @param  Request $request
	 * @param int $id
	 * @return Illuminate\Http\Response
	 */
	public function index(Request $request, $id)
	{
		$user = $this->users->byId($id);
		$comments = $user->comments()
											->with('post')
											->orderBy('created_at', 'desc')
											->paginate(15);

		return view('user.comments', compact('comments', 'user'));
	}
}
