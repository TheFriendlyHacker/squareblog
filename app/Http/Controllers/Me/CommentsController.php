<?php

namespace App\Http\Controllers\Me;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Repositories\UserRepository;

class CommentsController extends Controller
{
	/**
	 * UserRepository
	 */
	protected $users;

	/**
	 * Class constructor.
	 *
	 * @param UserRepository $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->users = $users;
	}

	/**
	 * List all of the current user's comments.
	 *
	 * @param  Request $request
	 * @return Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$user = $this->users->currentUser();
		$comments = $user->comments()
											->with('post')
											->orderBy('created_at', 'desc')
											->paginate(15);

		return view('me.comments', compact('comments', 'user'));
	}
}
