<?php

namespace App\Http\Controllers\Me;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Repositories\UserRepository;

class PostsController extends Controller
{
	/**
	 * UserRepository
	 */
	protected $users;

	/**
	 * Class constructor.
	 *
	 * @param UserRepository $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->users = $users;
	}

	/**
	 * List all of the current user's comments.
	 *
	 * @param  Request $request
	 * @return Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$user = $this->users->currentUser();

		if(!$user->is_admin) {
			return redirect(route('me.profile'));
		}

		$posts = $user->posts()
											->with('comments')
											->orderBy('created_at', 'desc')
											->paginate(10);

		return view('me.posts', compact('posts', 'user'));
	}
}
