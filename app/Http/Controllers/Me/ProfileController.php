<?php

namespace App\Http\Controllers\Me;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Contracts\Repositories\UserRepository;
use Illuminate\Validation\Rule;
use Auth;

class ProfileController extends Controller
{
	/**
	 * UserRepository
	 */
	protected $users;

	/**
	 * Class constructor.
	 *
	 * @param UserRepository $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->middleware('auth');
		$this->users = $users;
	}

  /**
   * Show the current authenticated user's profile.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function edit(Request $request)
  {
  	$user = $this->users->currentUser();

  	return view('me.profile', compact('user'));
  }

  /**
   * Update the current user's profile.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function update(Request $request)
  {
  	$user = Auth::user();

    $this->validateProfile($request);

    $user->update([
      'first_name' => $request->first_name,
      'last_name' => $request->last_name,
      'email' => $request->email
    ]);

    return redirect(route('me.profile'))
                  ->with('success', trans('messages.user.profile-successfully-updated'));
  }

  /**
   * Update the user's "about me" section.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function updateAbout(Request $request)
  {
    $user = Auth::user();

    $this->validateAbout($request);

    $user->update([
      'about' => $request->about
    ]);

    return redirect(route('me.profile'))
                  ->with('success', trans('messages.user.profile-successfully-updated'));
  }

  /**
   * Validate incoming data to update the user's profile.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validateProfile(Request $request)
  {
    $this->validate($request, [
      'first_name' => 'required|string|max:255',
      'last_name' => 'required|string|max:255',
      'email' => [ 'required', 'email', 'max:255', Rule::unique('users')->ignore(Auth::id()), ],
    ]);
  }

  /**
   * Validate incoming data to update the "about me" section.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validateAbout(Request $request)
  {
    $this->validate($request, [
      'about' => 'required'
    ]);
  }
}
