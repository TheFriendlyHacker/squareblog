<?php

namespace App\Http\Controllers\Me;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Repositories\UserRepository;
use App\User;

use Hash;

class PasswordController extends Controller
{
	/**
	 * UserRepository
	 */
	protected $users;

	/**
	 * Class constructor.
	 *
	 * @param UserRepository $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->middleware('auth');
		$this->users = $users;
	}

	/**
	 * Show the screen that allows the user to change their password.
	 *
	 * @param  Request $request
	 * @return Illuminate\Http\Response
	 */
	public function edit(Request $request)
	{
		$user = $this->users->currentUser();

		return view('me.password', compact('user'));
	}

	/**
	 * Change the current user's password.
	 *
	 * @param  Request $request
	 * @return Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$user = $this->users->currentUser();

		$this->validatePassword($request);

		if(!$this->currentPasswordIsCorrect($user, $request->current_password)) {
			return redirect(route('me.security'))
										->withError(trans('messages.user.current-password-invalid'));
		}

		$this->changePassword($user, $request->password);

		return redirect(route('me.security'))
									->with('success', trans('messages.user.password-successfully-changed'));
	}

	/**
	 * Validate incoming data for changing the password.
	 *
	 * @param  Request $request
	 * @return void
	 * @throws Illuminate\Validation\ValidationException
	 */
	protected function validatePassword(Request $request)
	{
		$this->validate($request, [
			'current_password' => 'required',
			'password' => 'required|string|min:6|confirmed'
		]);
	}

	/**
	 * Check that the "current password" the user entered is correct.
	 *
	 * @param  string $password
	 * @return boolean
	 */
	protected function currentPasswordIsCorrect(User $user, $password)
	{
		return Hash::check($password, $user->password);
	}

	/**
	 * Change the user's password.
	 *
	 * @param  string $password
	 * @return void
	 */
	protected function changePassword(User $user, $password)
	{
		$user->update([
			'password' => bcrypt($password)
		]);
	}
}
