<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contracts\Repositories\{ PostRepository, CommentRepository, UserRepository };

class CommentsController extends Controller
{
  /**
   * UserRepository
   */
  protected $users;
  /**
   * PostRepository
   */
  protected $posts;
  /**
   * CommentRepository
   */
  protected $comments;

  /**
   * Class constructor.
   *
   * @param UserRepository $users
   * @param PostRepository $posts
   * @param CommentRepository $comments
   */
  public function __construct(UserRepository $users, PostRepository $posts, CommentRepository $comments)
  {
    $this->users = $users;
    $this->posts = $posts;
    $this->comments = $comments;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param int $postId
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, $postId)
  {
    if(!settings('allow_comments')) return redirect(route('home'));

    $post = $this->posts->byId($postId);

    $this->validateComment($request);

    $comment = $this->comments->create($this->users->currentUser(), $post, $request->all());

    return redirect(route('post.show', $post->id))
            ->with('success', trans('messages.comments.successfully-created'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Request $request
   * @param  int  $postId
   * @param int $commentId
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $postId, $commentId)
  {
    if(!settings('allow_comments')) return redirect(route('home'));

    $user = $this->users->currentUser();
    $post = $this->posts->byId($postId);
    $comment = $user->comments()->findOrFail($commentId);

    $comment->delete();

    return redirect(url()->previous())
            ->with('success', trans('messages.comments.successfully-deleted'));
  }

  /**
   * Validate incomming data for submitting a new comment.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validateComment(Request $request)
  {
    $this->validate($request, [
      'body' => 'required'
    ]);
  }
}
