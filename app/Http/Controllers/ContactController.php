<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\NewContactRequest;
use Mail;

class ContactController extends Controller
{
	/**
	 * Display the contact page.
	 *
	 * @param  Request $request
	 * @return Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		if(!settings('show_contact_page')) abort(404);

		return view('contact');
	}

	/**
	 * Process and send a contact request.
	 *
	 * @param  Request $request
	 * @return Illuminate\Http\Response
	 */
	public function sendContactRequest(Request $request)
	{
		$this->validateContactRequest($request);

		$this->sendEmail($request);

		return redirect(route('contact'))
									->with('success', trans('messages.contact.request_sent'));
	}

	/**
	 * Validate incoming data for a contact request.
	 *
	 * @param  Request $request
	 * @return void
	 * @throws Illuminate\Validation\ValidationException
	 */
	protected function validateContactRequest(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'phone' => 'nullable|numeric',
			'message' => 'required|max:3072'
		]);
	}

	/**
	 * Send the contact request.
	 *
	 * @param  Request $request
	 * @return void
	 */
	protected function sendEmail(Request $request)
	{
		Mail::to(settings('contact_email'))->send(new NewContactRequest($request->all()));
	}
}
