<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Repositories\SettingsRepository;

class SettingsController extends Controller
{
  /**
   * SettingsRepository
   */
  protected $settings;

	/**
	 * Class constructor.
   *
   * @param SettingsRepository $settings
	 */
  public function __construct(SettingsRepository $settings)
  {
    $this->middleware('admin');
  	$this->settings = $settings;
  }

  /**
   * Display a listing of settings for the application.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $settings = $this->settings->get(['app_name', 'app_theme', 'home_title', 'allow_comments', 'from_email', 'from_name']);
    $themes = themes();

  	return view('admin.settings', compact('settings', 'themes'));
  }

  /**
   * Update the application's settings.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $this->validateSettings($request);

    $settings = $this->settings->update($request->only('app_name', 'app_theme', 'home_title', 'allow_comments', 'from_name', 'from_email'));

    return redirect(route('admin.settings'))
              ->with('success', trans('messages.admin.settings-updated'));
  }

  /**
   * Validate incoming data to update the settings.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validateSettings(Request $request)
  {
    $messages = [];

    $this->validate($request, [
      'app_name' => 'required|max:32|string',
      'app_theme' => 'required|in:'.implode(',', themes()),
      'home_title' => 'required|max:32|string',
      'allow_comments' => 'required|in:1,0'
    ]);
  }
}
