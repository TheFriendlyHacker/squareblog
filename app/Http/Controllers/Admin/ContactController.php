<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Repositories\SettingsRepository;

class ContactController extends Controller
{
  /**
   * SettingsRepository
   */
  protected $settings;

	/**
	 * Class constructor.
   *
   * @param SettingsRepository $settings
	 */
  public function __construct(SettingsRepository $settings)
  {
    $this->middleware('admin');
  	$this->settings = $settings;
  }

  /**
   * Display settings for the website's contact page.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function index(Request $request)
  {
  	$settings = $this->settings->get(['show_contact_page', 'contact_email']);

  	return view('admin.contact', compact('settings'));
  }

  /**
   * Update the application's contact page settings.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $this->validateSettings($request);

    $settings = $this->settings->update($request->only('show_contact_page', 'contact_email'));

    return redirect(route('admin.contact'))
              ->with('success', trans('messages.admin.contact-updated'));
  }

  /**
   * Validate incoming data to update the settings.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validateSettings(Request $request)
  {
    $messages = [
    	'contact_email.required_if' => trans('messages.admin.errors.contact_email_required_with_page')
    ];

    $this->validate($request, [
      'show_contact_page' => 'required|in:1,0',
      'contact_email' => 'nullable|required_if:show_contact_page,1|email|max:255'
    ], $messages);
  }

}
