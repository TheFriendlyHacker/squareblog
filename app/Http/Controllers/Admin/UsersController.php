<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Repositories\UserRepository;

class UsersController extends Controller
{
	/**
	 * @var UserRepository
	 */
  protected $users;

  /**
   * Class constructor.
   *
   * @param UserRepository $users
   */
  public function __construct(UserRepository $users)
  {
  	$this->middleware('admin');
  	$this->users = $users;
  }

  /**
   * Display a listing of the application's users.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function index(Request $request)
  {
  	if(!$request->has('q')) {
  		$users = $this->users->all(15);
  		$user_count = $this->users->all()->count();
  	} else {
  		$users = $this->users->bySearchQuery($request->q, 15);
  		$user_count = $users->count();
  	}

  	return view('admin.users', compact('users', 'user_count'));
  }
}
