<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use App\Contracts\Repositories\SettingsRepository;

class AboutController extends Controller
{
  /**
   * SettingsRepository
   */
  protected $settings;

	/**
	 * Class constructor.
   *
   * @param SettingsRepository $settings
	 */
  public function __construct(SettingsRepository $settings)
  {
    $this->middleware('admin');
  	$this->settings = $settings;
  }

  /**
   * Display a listing of settings for the application.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $settings = $this->settings->get(['show_about_page', 'about_title', 'about_body', 'about_image']);

  	return view('admin.about', compact('settings'));
  }

  /**
   * Update the application's settings.
   *
   * @param  Request $request
   * @return Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $this->validateSettings($request);

    $imagePath = null;
    if($request->hasFile('about_image')) {
    	$aboutImage = $this->storeImage($request->file('about_image'));
    }

    $update = $request->only('show_about_page', 'about_title', 'about_body');
    if(isset($aboutImage)) $update = array_merge($update, ['about_image' => $aboutImage]);

    $settings = $this->settings->update($update);

    return redirect(route('admin.about'))
              ->with('success', trans('messages.admin.about-updated'));
  }

  /**
   * Validate incoming data to update the settings.
   *
   * @param  Request $request
   * @return void
   * @throws Illuminate\Validation\ValidationException
   */
  protected function validateSettings(Request $request)
  {
    $messages = [
    	'about_title.required_if' => trans('messages.admin.errors.about_title_required_with_page'),
    	'about_body.required_if' => trans('messages.admin.errors.about_body_required_with_page')
    ];

    $this->validate($request, [
      'show_about_page' => 'required|in:0,1',
      'about_title' => 'nullable|required_if:show_about_page,1|max:32',
      'about_body' => 'nullable|required_if:show_about_page,1',
      'about_image' => 'nullable|dimensions:min_width=750,min_height=450|mimes:jpg,jpeg,png,gif,bmp'
    ], $messages);
  }

  /**
   * Save the given image to storage.
   *
   * @param  UploadedFile $file
   * @return string             asset path to the stored file.
   */
  protected function storeImage(UploadedFile $file)
  {
  	$path = $file->store(config('squareblog.settings_file_location'));
  	$pathArray = explode('/', $path);
  	$fileName = end($pathArray);
  	$symPathArray = explode('/', config('squareblog.settings_file_location'));
  	$symPath = end($symPathArray);
  	return 'storage/' . $symPath . '/' . $fileName;
  }
}
