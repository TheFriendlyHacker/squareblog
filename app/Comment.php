<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	/**
	 * Attributes that are not mass-assignable.
	 */
	protected $guarded = [];
	/**
	 * Date fields (returns a Carbon\Carbon instance).
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	/**
	 * Retrieve the user who created the comment.
	 *
	 * @return User
	 */
  public function user()
  {
  	return $this->belongsTo(User::class);
  }

	/**
	 * Retrieve the post that the comment was created on.
	 *
	 * @return User
	 */
  public function post()
  {
  	return $this->belongsTo(Post::class);
  }
}
