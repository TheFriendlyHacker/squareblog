<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    Schema::defaultStringLength(191);
  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    $this->registerRepositories();
  }

  /**
   * Register repository classes and bind them to their contracts.
   *
   * @return void
   */
  protected function registerRepositories()
  {
    $repositories = [
      \App\Contracts\Repositories\UserRepository::class => \App\Repositories\UserRepository::class,
      \App\Contracts\Repositories\PostRepository::class => \App\Repositories\PostRepository::class,
      \App\Contracts\Repositories\CommentRepository::class => \App\Repositories\CommentRepository::class,
      \App\Contracts\Repositories\SettingsRepository::class => \App\Repositories\SettingsRepository::class,
    ];

    foreach($repositories as $contract => $repository)
    {
      $this->app->bind($contract, $repository);
    }
  }
}
