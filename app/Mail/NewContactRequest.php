<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewContactRequest extends Mailable
{
  use SerializesModels;

  /**
   * @var Array
   */
  public $data;

  /**
   * Create a new message instance.
   *
   * @param array $data
   * @return void
   */
  public function __construct(array $data)
  {
    $this->data = $data;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->from(settings('from_email'), settings('from_name'))->markdown('emails.new-contact-request');
  }
}
